from charms.reactive import when, when_any, when_not
from charms.reactive import set_flag, clear_flag
from charms.reactive import Endpoint
from charms.reactive import data_changed

class DruidConfigProvides(Endpoint):

    # This endpoint corresponds to the Druid Charms end.

    @when('endpoint.{endpoint_name}.joined')
    def _handle_joined(self):
        set_flag(self.expand_name('{endpoint_name}.connected'))

    @when('endpoint.{endpoint_name}.changed.config')
    def new_config(self):
        set_flag(self.expand_name('endpoint.{endpoint_name}.new_config'))
        clear_flag(self.expand_name('endpoint.{endpoint_name}.changed.config'))

    @when('endpoint.{endpoint_name}.changed.hdfs_files')
    def new_hdfs_files(self):
        set_flag(self.expand_name('endpoint.{endpoint_name}.new_hdfs_files'))
        clear_flag(self.expand_name('endpoint.{endpoint_name}.changed.hdfs_files'))

    @when_not('endpoint.{endpoint_name}.joined')
    def _handle_broken(self):
        clear_flag(self.expand_name('{endpoint_name}.connected'))

    def get_config(self):
        """
        Get the config file that has been published over the relation.
        """

        config_file = ""

        for relation in self.relations:
            for unit in relation.units:
                config_file = config_file + unit.received['config']
        return config_file

    def get_hadoop_files(self):
        """
        Get the Hadoop Config XML strings that have been sent over the relation.
        """

        for relation in self.relations:
            for unit in relation.units:
                return unit.received['hdfs_files']
