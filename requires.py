from charms.reactive import Endpoint
from charms.reactive import set_flag, clear_flag
from charms.reactive import when, when_not

class DruidConfigRequires(Endpoint):

    # This endpoint corresponds to the druid-config end.

    @when('endpoint.{endpoint_name}.joined')
    def _handle_joined(self):
        set_flag(self.expand_name('{endpoint_name}.joined'))

    @when_not('endpoint.{endpoint_name}.joined')
    def _handle_broken(self):
        clear_flag(self.expand_name('{endpoint_name}.joined'))

    def provide_config(self, config):
        for relation in self.relations:
            relation.to_publish['config'] = config

    def provide_hadoop_files(self, files):
        for relation in self.relations:
            relation.to_publish['hdfs_files'] = files